import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './features/home/home.component';
import { PersonajesOPComponent } from './features/personajes-op/personajes-op.component';
import { PersonajeDetalleComponent } from './features/personaje-detalle/personaje-detalle.component';

const routes: Routes = [
  {path: '', component: HomeComponent, pathMatch: 'full'},
  {path:'personajes/:idmov',component:PersonajesOPComponent, pathMatch:'full'},
  {path:'personaje_detalle/:idper',component:PersonajeDetalleComponent, pathMatch:'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabledBlocking'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
