import { TestBed } from '@angular/core/testing';

import { MovieOnepieceService } from './movie-onepiece.service';

describe('MovieOnepieceService', () => {
  let service: MovieOnepieceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MovieOnepieceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
