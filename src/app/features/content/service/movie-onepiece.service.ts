import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MovieOnepieceService {

  constructor(private http: HttpClient) { }

  listarPeliculas():Observable<any>{
    const url = `https://api.jikan.moe/v4/anime?q=one piece&type=Movie`;
    return this.http.get(`${url}`)
  }
  listarPersonajes(idPelicula:number):Observable<any>{
    const url = `https://api.jikan.moe/v4/anime/${idPelicula}/characters`;
    return this.http.get(`${url}`)
  }
  listarPersonajeDetalle(idPersonaje:number):Observable<any>{
    const url = `https://api.jikan.moe/v4/characters/${idPersonaje}/full`;
    return this.http.get(`${url}`)
  }
}
