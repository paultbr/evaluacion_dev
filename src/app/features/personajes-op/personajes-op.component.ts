import { Component,OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MovieOnepieceService } from '../content/service/movie-onepiece.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-personajes-op',
  templateUrl: './personajes-op.component.html',
  styleUrls: ['./personajes-op.component.scss'],
  providers:[MovieOnepieceService]
})
export class PersonajesOPComponent implements OnInit{

  listaPersonajes:any;
  datosPersonaje:any;

  constructor(private rutaActiva: ActivatedRoute, private movieonepieceservice:MovieOnepieceService,private router: Router){}

  ngOnInit(): void {
    this.listarPersonajesPelicula();
  }

  listarPersonajesPelicula(){
    let datos:any=this.rutaActiva.snapshot.params;
    this.movieonepieceservice.listarPersonajes(datos.idmov)
    .subscribe((result)=>{
      this.listaPersonajes=result.data;
    });
  }
  listarDatosPersonaje(idPersonaje:number){
    console.log(idPersonaje);
    let datos:any=this.rutaActiva.snapshot.params;
    this.movieonepieceservice.listarPersonajeDetalle(idPersonaje)
    .subscribe((result)=>{
      this.datosPersonaje=result.data;
      console.log(this.datosPersonaje);
    });
  }

  verPersonajeDetalle(idPersonaje:number){
    this.router.navigate(['/personaje_detalle/'+idPersonaje]);
  }

}
