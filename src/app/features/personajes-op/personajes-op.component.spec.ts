import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonajesOPComponent } from './personajes-op.component';

describe('PersonajesOPComponent', () => {
  let component: PersonajesOPComponent;
  let fixture: ComponentFixture<PersonajesOPComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonajesOPComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PersonajesOPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
