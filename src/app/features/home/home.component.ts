import { Component, OnInit } from '@angular/core';
import { MovieOnepieceService } from 'src/app/features/content/service/movie-onepiece.service';
import { peliculasOP } from '../content/model/peliculas.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers:[MovieOnepieceService]
})
export class HomeComponent implements OnInit{

  public listaPeliculas:Array<peliculasOP>=[];
  public listaPeliculas2:any;

  constructor(private movieonepieceservice:MovieOnepieceService,private router: Router){}

  ngOnInit(): void {
    this.listarPeliculasOnePiece();

  }

  listarPeliculasOnePiece(){
    this.movieonepieceservice.listarPeliculas()
    .subscribe((result)=>{
      this.listaPeliculas2=result.data;
      console.log(this.listaPeliculas2);
    });
  }

  verPersonajes(idPelicula:number){
    this.router.navigate(['/personajes/'+idPelicula]);
  }


}
