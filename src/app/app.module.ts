import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './features/home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PersonajesOPComponent } from './features/personajes-op/personajes-op.component';
import { PersonajeDetalleComponent } from './features/personaje-detalle/personaje-detalle.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PersonajesOPComponent,
    PersonajeDetalleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
